<h1> Tools </h1>

- [**PyJapcScout**](https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout)
	- The purpose of PyJapcScout is to explore the possibility of a more convenient interface, compared to PyJapc, for MD studies.	
    - Functionalities include group subscription, saving data in various formats and setting functions.

- [**tune-spread**](https://github.com/fasvesta/tune-spread)
    - A module to analytically estimate the space charge tune spread up to 3 beam sigmas using pre-estimated potentials from PySCRDT.

- [**PySCRDT**](https://github.com/fasvesta/PySCRDT)
    - A module to calculate the resonance driving terms from the space charge potential. 
    - A technical description of the tool is available in this note: [CERN-ACC-NOTE-2019-0046](https://cds.cern.ch/record/2696190?ln=en)

- [**PyNAFF**](https://pypi.org/project/PyNAFF/)
    - A Python module that implements the Numerical Analysis of Fundamental Frequencies method of J. Lashkar (https://www.sciencedirect.com/science/article/pii/001910359090084M). The code works either as a script (as the original code of Lashkar) or loaded as a module in Python/Julia code or jupyter-like notebooks (i.e. SWAN).

- [**beamtools**](https://gitlab.cern.ch/tprebiba/beam-analysis-sandbox/-/tree/master/beamtools)
    - A set of python functions for analyzing MD data. 
    - It includes functions for emittance calculation, profile fitting, dispersion deconvolution, longitudinal profile extraction (from .dat files of Tomo application), FFTs and more with some examples. 