<h1> Mandate </h1>

* Investigate limits of the machine performance of the LHC proton and ion Injectors as well as in AD and Elena in the presence of space charge, cooling and intra-beam scattering and their interplay with other nonlinearities.
* Address issues of emittance blow-up, losses and tail creation in the different machines in the context of operational users and future projects.
* Further develop the nonlinear models for the CERN accelerators in simulation tools and compare them to measurements.
* Develop and benchmark new simulation codes, such as Xsuite versus established codes and experimental data.
* Collaborate with other laboratories to exchange ideas and expertise on issues of space charge, cooling and intra-beam scattering.
