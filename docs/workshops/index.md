<h1> Space Charge Workshops </h1>
- [6th ICFA Mini-Workshop on Space Charge 2024 (Dong Guan, China)](https://indico.ihep.ac.cn/event/21466/)
- [5th ICFA Mini-Workshop on Space Charge 2022 (Oak Ridge, Knoxville, USA)](https://conference.sns.gov/event/335/)
- [4th ICFA Mini-Workshop on Space Charge 2019 (CERN, Geneva)](https://indico.cern.ch/event/828559/)
- [2nd CERN Space Charge Collaboration Meeting 2018 (CERN, Geneva)](https://indico.cern.ch/event/688897/)
- [Space Charge 2017 (GSI, Darmstadt, Germany)](https://indico.gsi.de/event/5600/)
- [Space Charge 2015 (Oxford, UK)](http://eucard2.web.cern.ch/events/space-charge-2015)
- [1st CERN Space Charge Collaboration Meeting 2014 (CERN, Geneva)](https://indico.cern.ch/event/292362/)
- [Space Charge 2013 (CERN, Geneva)](https://indico.cern.ch/event/221441/)
