<h1> Using HPC-Batch </h1>

## HPC-Batch

- A batch HPC cluster using the SLURM (Simple Linux Utility for Resource Management) workload manager has been set up for users of MPI (Message Passing Interface) applications that are massively parallel and computationally intensive.
- Access is restricted to approved HPC (High Performance Computing) users, mainly from the Accelerator and Technology sector. PyORBIT, a Python/C++ implementation of the ORBIT (Objective Ring Beam Injection and Tracking) code used at CERN for space charge simulations, is well suited to this system.
- Official instructions for accessing the HPC-Batch system are available in the knowledge base article KB0004541: ”How to access and use the batch HPC cluster”: https://cern.service-now.com/service-portal/article.do?n=KB0004541
- In order to gain access one must request this from the ABP Computing Working Group. Email Xavier Buffat, Giovanni Rumolo, or Giovanni Iadarola (correct on 28.08.20) to request access.

## Important Notes

- The HPC-Batch system should only be used where HTCondor is not possible. For use of the slice-by-slice space charge algorithm HTCondor is not usually sufficient.
- Where possible jobs submitted should use 2 nodes or more. Single node jobs should use the 32-core HTCondor queue.
- The PTC-PyORBIT custom environment is accessible via AFS on HPC-Batch, and is called via the 'setup_environment.sh' file included in examples.
- No local intensive operations should be performed on the HPC-Batch submission nodes. All work should be submitted as jobs. Do not for example run plotting scripts on submisison nodes, instead submit them as jobs.
- User data is saved on /hpcscratch - each user has limited space as this memory is shared. It is reccomended to move all simulation outputs to EOS.
- Where possible always test that your PyORBIT simulation runs for 1 turn on LXPlus (using reduced space charge or bunch paramters if necessary) before submitting on HPC-Batch.

## Accessing HPC-Batch

- One may SSH (Secure SHell: cryptographic network protocol for operating network services securely over an unsecured network) into HPC-Batch.

- From inside the CERN network:
```bash
ssh -XY your_username@hpc-batch.cern.ch
```

- From outside the CERN network one can access HPC-Batch from within LXPlus:
```bash
ssh -XY your_username@lxplus.cern.ch
ssh -XY your_username@hpc-batch.cern.ch
```

- Useful shortcut - put this in your ~/.bashrc file to shorten the entire command to 'lxp' or 'hpcb':
```bash
ssh_lxplus_function(){
    ssh -XY your_username@lxplus.cern.ch
}
alias lxp=ssh_lxplus_function

ssh_hpc-batch_function(){
    ssh -XY your_username@hpc-batch.cern.ch
}
alias hpcb=ssh_hpc-batch_function
```

## Using HPC-Batch

- It is recommended to use a GitLab or GitHub respository when performing multiple large space charge simulations, though it is not necessary.
- Once one has cloned their repository containing the PyORBIT simulations (see [PyORBIT_Examples](./PyORBIT_Examples.md) for more information) the following commands are useful:

### The following SLURM commands are useful:
- **squeue:** view job and job step information, -u <user> displays a specific users jobs. ![image info](./HPC_Batch/squeue.png)
- **squeue -u <user>:** displays a specific users jobs will be empty if user has no jobs). ![image info](./HPC_Batch/squeue2.png)
- **sacct:** displays accounting data for all obs and job steps in the Slurm accounting log or Slurm database.
- **sbatch:** submit a batch script to Slurm.
- **srun:** run parallel jobs (used similarly to mpirun).
- **scancel:** used with a job ID to kill the specified job.
- **sinfo:** displays the status of all available nodes, an example of this command on theHPC-Batch system. ![image info](./HPC_Batch/sinfo.png)
- **sreport:** generate reports from the SLURM accounting data. ![image info](./HPC_Batch/sr.png)
- **sshare:** display fairshare data. ![image info](./HPC_Batch/sshare.png)

## Job submission

<details>
<summary> In a bash script, the following #SBATCH commands may be used to specify job parameters in a SLURM submission script (written in bash): </summary>
<ul>
<li> <b>--job-name</b>: job name.</li>
<li> <b>--output</b>: the  output  file  created  (not  from  PyORBIT).  This  file  will  record  what  wouldnormally be output to screen/console.  A useful format is name.%N.%j.out, where %Nis the node number, and %j is the Job ID.</li>
<li> <b>--error</b>: the  error  file  created  (not  from  PyORBIT).  This  file  will  record  what  wouldnormally be output to screen/console.  A useful format is name.%N.%j.out, where %Nis the node number, and %j is the Job ID.</li>
<li> <b>--nodes</b>: number of nodes, each node has threads over 20 cpus on HPC-Batch,  and the number of nodes does not need to be specified, only the number of tasks.</li>
<li> <b>--ntasks-per-node</b>: number of tasks per node (threads).</li>
<li> <b>--partition</b>: queue name, for HPC-Batch the options are inf-short, inf-long, batch-short, batch-long.</li>
<li> <b>--time</b>: wall-clock time limit for the job, in the format day-hour:minute:second, for example1 -10:00:00.  This is a hard limit, if exceeded your job will be cancelled by Slurm.</li>
<li> <b>--mem-per-cpu</b>: the RAM allocation for this job, for example 12gb.</li>
<li> <b>--exclusive</b>: flag to have exclusive use of allocated nodes.</li>
<li> <b>--hint=nomultithread</b>: disable hyper-threading.</li>
<ul>
    
For example:
```bash
#!/bin/bash
#SBATCH --job-name=1_H_10
#SBATCH --output=slurm.%N.%j.out
#SBATCH --error=slurm.%N.%j.err
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=20
#SBATCH --partition=inf-short
#SBATCH --time=120:00:00
#SBATCH --mem-per-cpu=3200M
#SBATCH --exclusive
#SBATCH --hint=nomultithread

BATCH_ROOT_DIR=/hpcscratch/user/your_username
RUN_DIR=/hpcscratch/user/your_username/Simulation_Repository/Simulation_Directory
OrigIwd=$(pwd)

# Make an output folder in the root directory to hold SLURM info file
cd ${BATCH_ROOT_DIR}
output_dir="output"
mkdir -p $output_dir

# Fill the SLURM info file
simulation_info_file="${BATCH_ROOT_DIR}/${output_dir}/simulation_info_${SLURM_JOB_ID}.${SLURM_NODEID}.${SLURM_PROCID}.txt"
echo "PyOrbit path:  `readlink -f ${ORBIT_ROOT}`" >> ${simulation_info_file}
echo "Run path:  `readlink -f ${RUN_DIR}`" >> ${simulation_info_file}
echo "Submit host:  `readlink -f ${SLURM_SUBMIT_HOST}`" >> ${simulation_info_file}
echo "SLURM Job name:  `readlink -f ${SLURM_JOB_NAME}`" >> ${simulation_info_file}
echo "SLURM Job ID:  `readlink -f ${SLURM_JOB_ID}`" >> ${simulation_info_file}
echo "SLURM Nodes allocated:  `readlink -f ${SLURM_JOB_NUM_NODES}`" >> ${simulation_info_file}
echo "SLURM CPUS per Node:  `readlink -f ${SLURM_CPUS_ON_NODE}`" >> ${simulation_info_file}
echo "SLURM Node ID:  `readlink -f ${SLURM_NODEID}`" >> ${simulation_info_file}
echo "SLURM total cores for job:  `readlink -f ${SLURM_NTASKS}`" >> ${simulation_info_file}
echo "SLURM process ID:  `readlink -f ${SLURM_PROCID}`" >> ${simulation_info_file}
echo "****************************************" >> ${simulation_info_file}

# Enter job directory, clean it, and setup environment -> SLURM info file
cd ${RUN_DIR}
./clean_all.sh
. setup_environment.sh >> ${simulation_info_file}

# Load latest MPI
module load mpi/mvapich2/2.3

tstart=$(date +%s)

# Run the job
srun --hint=nomultithread ${ORBIT_ROOT}/bin/pyORBIT ${RUN_DIR}/pyOrbit.py

tend=$(date +%s)
dt=$(($tend - $tstart))
echo "total simulation time (s): " $dt >> ${simulation_info_file}
```

If the script is called 'SLURM_submisison_script.sh', it should be submitted as:
```bash 
sbatch SLURM_submisison_script.sh
```
</details>

It should be noted that most PyORBIT examples contain a Make\_SLURM\_submission\_script.py script which will automatically generate the simulation specific SLURM submission script. For simulation campaigns with many simulations it is useful to automate this process based on the simulation folder name and call these scripts from a single script (usually provided in examples as Submit\_All.py).