# Space Charge Simulation Codes

### Xsuite
- Suite of python packages for multiparticle simulations for particle accelerators.
- Documentation is on: https://xsuite.readthedocs.io/en/latest/.
- Space Charge related examples:
  - PSB lattice with nonlinear elements for benchmarking studies: https://github.com/fasvesta/benchmarking_psb
  - PS ions example: https://gitlab.cern.ch/elwaagaa/ps_space_charge_example/
  - SPS protons example: https://github.com/xsuite/xtrack/tree/main/examples/spacecharge 
  - SPS ions example: https://gitlab.cern.ch/elwaagaa/sps_ion_space_charge_example
  - PSB realistic tracking simulation example: https://github.com/tprebiba/psb-xsuite-tracking
    - Modelling of the injection bump and perturbations, foil scattering, multi-turn injection of L4 distribution, acceleration in double or triple harmonic, imperfections, dynamic tune ramp and space charge. 
    - It includes basic analysis tools and relevant files for GPU sumbission on HTCondor. 

### PyORBIT

- Macro-particle simulation code mainly developed by S. Cousineau, J. Holmes and A. Shishlo (SNS Oak Ridge) with contributions from colleagues at CERN and GSI https://doi.org/10.1016/j.procs.2015.05.312
- Computational beam dynamics models for accelerators, main focus on space charge effects and related issues (H- injection with foil scattering, collimation, etc) for a single bunch.
- Modernised version of the ORBIT code with Python as user interface. ORBIT was developed at Oak Ridge National Laboratory (ORNL) for designing the Spallation Neutron Source (SNS) accelerator complex.
- Open source, available at GitHub https://github.com/PyORBIT-Collaboration
- CERN uses a modified version shared here: https://github.com/hannes-bartosik/py-orbit
- Examples of using PyORBIT for CERN in: /eos/project/p/pyorbit/public/Examples

### PTC

- Polymorphic Tracking Code written by E. Forest et. al., https://cds.cern.ch/record/573082/files/sl-2002-044.pdf
- CERN uses a modified version shared here: https://github.com/hannes-bartosik/PTC

### PTC-PyORBIT

- PTC-PyORBIT (and the required virtual environment dependencies including python 2.7) is currently available (June 2020) at CERN on AFS in the following directory: /afs/cern.ch/user/p/pyorbit/public/PyOrbit_env
- The latest version of PyORBIT at CERN is always located at: /afs/cern.ch/user/p/pyorbit/public/PyOrbit_env/py-orbit


# PTC-PyORBIT User Guide

## Running PyORBIT
- PTC-PyORBIT is run in a custom virtual environment (note this includes Python 2.7) which is made available at CERN via AFS: /afs/cern.ch/user/p/pyorbit/public/PyOrbit_env/py-orbit

The above link always points to the latest available version. PTC-PyORBIT may be initialised via the following bash script setup_environment.sh:

```bash
# script to setup the PyOrbit environment 
# execute like: . setup_environment.sh

pyOrbit_dir=/afs/cern.ch/user/p/pyorbit/public/PyOrbit_env/py-orbit

source ${pyOrbit_dir}/customEnvironment.sh
echo "customEnvironment done"
source ${pyOrbit_dir}/../virtualenvs/py2.7/bin/activate
echo "python packages charged"
source ${pyOrbit_dir}/../setup_ifort.sh
echo "ifort charged (necessary for running)"


ORBIT_ROOT_fullpath=`readlink -f ${ORBIT_ROOT}` 
echo 
echo "*****************************************************"
echo 
echo "full PyOrbit path:  ${ORBIT_ROOT_fullpath}"
echo
. ${ORBIT_ROOT}/../CheckGitStatus.sh ${ORBIT_ROOT_fullpath}
```

A PTC-PyORBIT simulation may then be run locally using the following script START_local.sh:

```bash
#!/bin/bash

set -o errexit

if [ ! -n "$1" ]
  then
    echo "Usage: `basename $0` <name of the SC script> <N CPUs>"
    exit $E_BADARGS
fi

if [ ! -n "$2" ]
  then
    echo "Usage: `basename $0` <name of the SC script> <N CPUs>"
    exit $E_BADARGS
fi

export FI_PROVIDER=sockets
. setup_environment.sh

mpirun -np $2 ${ORBIT_ROOT}/bin/pyORBIT $1
```

Using the command (here for example the PTC-PyORBIT simulation file is pyOrbit.py, and we execute 2 MPI processes):
```bash
./START_local pyOrbit.py 2
```


## Defining an accelerator lattice using acc-models
- CERN accelerator optics models are maintained in the acc-models GitLab repository found at https://gitlab.cern.ch/acc-models

## Creating a PTC flat file from the lattice in MAD-X/PTC</summary>
- This is readable by PTC-PyORBIT as an input, and defines the PTC-PyORBIT lattice. An example is given in the .ipynb in the following repository folder: https://github.com/HaroonRafique/PTC-PyORBIT_Examples/tree/master/Create_Flat_File
- Instructions can be viewed here: https://nbviewer.jupyter.org/github/HaroonRafique/PTC-PyORBIT_Examples/blob/master/Create_Flat_File/Instructions.ipynb



<details>
<summary> Creating a matched initial particle distribution in PyORBIT </summary>

<details>
<summary> Transverse </summary>
</details>

<details>
<summary> Longitudinal Parabolic </summary>
</details>

<details>
<summary> Longitudinal from Tomogram </summary>
</details>

<details>
<summary> Longitundinal using BLonD </summary>
</details>

</details>


<details>
<summary> Manipulating the lattice with PTC files</summary>

<details>
<summary> RF Cavity settings </summary>
</details>

<details>
<summary> Magnet ramping </summary>
</details>

</details>


<details>
<summary> Space Charge </summary>

<details>
<summary> Frozen space charge </summary>
</details>

<details>
<summary> PIC space charge </summary>
</details>

<details>
<summary> Longitudinal space charge </summary>
</details>

<details>
<summary> Installing space charge nodes into the lattice </summary>
</details>

</details>


<details>
<summary> Generating and handling the required output data </summary>

<details>
<summary> Output library </summary>
</details>

<details>
<summary> PTC Twiss </summary>
</details>

<details>
<summary> PyORBIT Lattice Functions </summary>
</details>

<details>
<summary> Bunch output file </summary>
</details>

</details>
