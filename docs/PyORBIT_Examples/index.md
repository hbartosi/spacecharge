<h1> PyORBIT </h1>

## PyORBIT

- Macro-particle simulation code mainly developed by S. Cousineau, J. Holmes and A. Shishlo (SNS Oak Ridge) with contributions from colleagues at CERN and GSI https://doi.org/10.1016/j.procs.2015.05.312
- Computational beam dynamics models for accelerators, main focus on space charge effects and related issues (H- injection with foil scattering, collimation, etc) for a single bunch.
- Modernised version of the ORBIT code with Python as user interface. ORBIT was developed at Oak Ridge National Laboratory (ORNL) for designing the Spallation Neutron Source (SNS) accelerator complex.
- Open source, available at GitHub https://github.com/PyORBIT-Collaboration
- CERN uses a modified version shared here: https://github.com/hannes-bartosik/py-orbit
- Examples of using PyORBIT for CERN in: /eos/project/p/pyorbit/public/Examples

## PTC

- Polymorphic Tracking Code written by E. Forest et. al., https://cds.cern.ch/record/573082/files/sl-2002-044.pdf
- CERN uses a modified version shared here: https://github.com/hannes-bartosik/PTC

## PTC-PyORBIT

- PTC-PyORBIT (and the required virtual environment dependencies including python 2.7) is currently available (June 2020) at CERN on AFS in the following directory: /afs/cern.ch/user/p/pyorbit/public/PyOrbit_env
- The latest version of PyORBIT at CERN is always located at: /afs/cern.ch/user/p/pyorbit/public/PyOrbit_env/py-orbit


<details>
<summary> Defining an accelerator lattice using acc-models </summary>
CERN accelerator optics models are maintained in the acc-models GitLab repository found at https://gitlab.cern.ch/acc-models
</details>


<details>
<summary> Creating a PTC flat file from the lattice in MAD-X/PTC</summary>
This is readable by PTC-PyORBIT as an input, and defines the PTC-PyORBIT lattice. 
</details>


<details>
<summary> Creating a matched initial particle distribution in PyORBIT </summary>

<details>
<summary> Transverse </summary>
</details>

<details>
<summary> Longitudinal Parabolic </summary>
</details>

<details>
<summary> Longitudinal from Tomogram </summary>
</details>

<details>
<summary> Longitundinal using BLonD </summary>
</details>

</details>


<details>
<summary> Manipulating the lattice with PTC files</summary>

<details>
<summary> RF Cavity settings </summary>
</details>

<details>
<summary> Magnet ramping </summary>
</details>

</details>


<details>
<summary> Space Charge </summary>

<details>
<summary> Frozen space charge </summary>
</details>

<details>
<summary> PIC space charge </summary>
</details>

<details>
<summary> Longitudinal space charge </summary>
</details>

<details>
<summary> Installing space charge nodes into the lattice </summary>
</details>

</details>


<details>
<summary> Generating and handling the required output data </summary>

<details>
<summary> Output library </summary>
</details>

<details>
<summary> PTC Twiss </summary>
</details>

<details>
<summary> PyORBIT Lattice Functions </summary>
</details>

<details>
<summary> Bunch output file </summary>
</details>

</details>
