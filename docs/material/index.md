<h1> Links and material </h1>

**LIU project**

* LIU project [TDR protons](https://edms.cern.ch/ui/file/1451384/4/CERN-ACC-2014-0337.pdf), [TDR ions](LIU TDR : Volume 2 - Ions)
* LIU beam parameter tables [protons](https://edms.cern.ch/file/1296306/2/LIU-table-protons_v3.pdf), [ions](LIU-Ions_beam_parameter_table.pdf)


